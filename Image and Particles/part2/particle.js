export class Particle {
	constructor(x, y, color, canvas, png) {

		//this.x =  x + canvas.width/2 - (png.width / 2 * 4)
		this.x = x + canvas.width/2 - (png.width * 2);
		this.y = y + canvas.height/2 - (png.height * 2);

		//this.y =  y + canvas.width/2 - (png.width / 2 * 4)
		this.color = color;
		this.size = 2;

		// x, y의 최초 위치를 기억한다.
		this.baseX = x + canvas.width/2 - (png.width * 2);
		this.baseY = y + canvas.height/2 - (png.height * 2);

		// 이부분은 잘 모르겟음 ????
		this.density = (Math.random() * 10) + 2;
	}

	draw (ctx) {
		ctx.beginPath();
		ctx.arc(this.x, this.y, this.size, 0, Math.PI * 2, false)
		ctx.fillStyle = this.color;
		ctx.closePath();
		ctx.fill();
	}

	update (ctx, mouse) {
		// collision detection
		// 양 점 사이의 거리를 구하고 (x, y)/ 거리로 나누면 단위 벡터를 구할수 있다.
		// https://m.blog.naver.com/PostView.nhn?blogId=ruvendix&logNo=221193707334&proxyReferer=https:%2F%2Fwww.google.com%2F

		let dx = mouse.x - this.x;
		let dy = mouse.y - this.y;
		let distance = Math.sqrt(dx * dx + dy * dy);
		let forceDirectionX = dx / distance;
		let forceDirectionY = dy / distance;


		const maxDistance = 100;

		// 거리가 멀어질수록 force 값음 줄어든다.
		let force = (maxDistance - distance) / maxDistance;
		if (force < 0) force = 0;

		let directionX = (forceDirectionX * force * this.density * 0.6);
		let directionY = (forceDirectionY * force * this.density * 0.6);


		// 마우스 반경 100픽셀 안에 있으면
		if (distance < mouse.radius + this.size) {
			this.x -= directionX;
			this.y -= directionY;
		} else {
			// 아니면 제자리로 다시 돌아온다.
			if (this.x !== this.baseX) {
				let dx = this.x - this.baseX;
				this.x -= dx/20;
			}
			if (this.y !== this.baseY) {
				let dy = this.y - this.baseY;
				this.y -= dy/20;
			}
		}

		this.draw(ctx);
	}
}