import {Particle} from './particle.js'
import ImageBase64 from './const.js'

class App {
	constructor() {
		this.canvas = document.createElement('canvas');
		document.body.appendChild(this.canvas);
		this.ctx = this.canvas.getContext('2d');

		this.pixelRatio = window.devicePixelRatio > 1 ? 2 : 1;
		this.particles = [];

		// mouse
		this.mouse = {
			x: null,
			y: null,
			radius: 100
		};

		window.addEventListener('resize', this.resize.bind(this), false);
		window.addEventListener('mousemove', this.mouseMove.bind(this), false);

		this.stageWidth = document.body.clientWidth;
		this.stageHeight = document.body.clientHeight;
		this.png = new Image();
		this.png.src = ImageBase64;

		this.png.onload = () => {
			this.canvas.width = this.stageWidth;
			this.canvas.height = this.stageHeight;

			// canvas에 이미지를 그린다.
			this.ctx.drawImage(this.png, 0, 0);
			this.drawImage();
			this.init();
			this.animate();
		};
	}
	resize() {
		this.stageWidth = document.body.clientWidth;
		this.stageHeight = document.body.clientHeight;

		this.canvas.width = this.stageWidth;
		this.canvas.height = this.stageHeight;

		this.init();
	}

	init () {
		// 100 * 100 이미지의 RGB 값을 추출한다.
		// 이미지 data rgba ->[0 ~ 3, 4 ~7, 8~ 12 ]
		for (let y = 0, y2 = this.data.height; y < y2; y++) {
			for (let x = 0, x2 = this.data.width; x < x2; x++) {
				if (this.data.data[(y * 4 * this.data.width) + (x * 4)] + 3 > 128) {
					let positionX = x;
					let positionY = y;
					let red = this.data.data[(y * 4 * this.data.width) + (x * 4)];
					let green = this.data.data[(y * 4 * this.data.width) + (x * 4 + 1)];
					let blue = this.data.data[(y * 4 * this.data.width) + (x * 4 + 2)];
					let color = `rgb(${red}, ${green}, ${blue})`;
					this.particles.push(new Particle(positionX * 4, positionY * 4, color, this.canvas, this.png))
				}
			}
		}
	}
	mouseMove (e) {
		this.mouse.x = e.clientX;
		this.mouse.y = e.clientY;
	}
	drawImage () {
		let imageWidth = this.png.width;
		let imageHeight = this.png.height;

		// canvas에 그린 이미지 data (Uint8ClampedArray)를 가져온다.
		this.data = this.ctx.getImageData(0, 0, imageWidth, imageHeight);

		// cavas에 그린 이미지를 지운다/
		this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
	}
	animate () {
		requestAnimationFrame(this.animate.bind(this));

		this.ctx.fillStyle = 'rgb(0, 0, 0, .05)';
		this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);


		for (let i = 0; i < this.particles.length; i++) {
			this.particles[i].update(this.ctx, this.mouse);
		}
	}
}

window.onload = () => {
	new App();
};