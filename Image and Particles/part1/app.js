import {Particle} from './particle.js'
import ImageBase64 from './const.js'

class App {
	constructor() {
		this.canvas = document.createElement('canvas');
		document.body.appendChild(this.canvas);
		this.ctx = this.canvas.getContext('2d');

		this.pixelRatio = window.devicePixelRatio > 1 ? 2 : 1;
		this.numberOfParticles = 3000;
		this.particles = [];


		this.png = new Image();
		this.png.src = ImageBase64;

		this.png.onload = () => {
			let imageWidth = this.png.width;
			let imageHeight = this.png.height;

			this.canvas.width = imageWidth;
			this.canvas.height = imageHeight;
			this.ctx.drawImage(this.png, 0, 0, this.canvas.width, this.canvas.height);
			this.drawImage();
			this.mappedImage();
			this.init();
			this.animate();
		};
	}
	init () {
		for (let i = 0; i < this.numberOfParticles; i++) {
			this.particles.push(new Particle(this.canvas));
		}
	}
	drawImage () {
		let imageWidth = this.png.width;
		let imageHeight = this.png.height;
		this.data = this.ctx.getImageData(0, 0, this.canvas.width, this.canvas.height);
		this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
	}
	animate () {
		this.ctx.globalAlpha = 0.05;
		this.ctx.fillStyle = 'rgb(0, 0, 0)';
		this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
		this.ctx.globalAlpha = 0.2;
		for (let i = 0; i < this.particles.length; i++) {
			this.particles[i].update(this.canvas, this.mappedImageArray);
			this.particles[i].draw(this.ctx);
		}
		requestAnimationFrame(this.animate.bind(this));
	}
	mappedImage () {
		this.mappedImageArray = [];
		for (let y = 0, y2 = this.data.height; y < y2; y++) {
			let row = [];
			for (let x = 0, x2 = this.data.width; x < x2; x++) {
				//*
				// 첫번째 픽셀 정보는 RGBA(data[0],data[1],data[2],data[3]).
				// 두번째 픽셀 정보는 RGBA(data[4],data[5],data[6],data[7]).
				// N 번째 픽셀 정보는 is RGBA(data[(n-1)*4],data[(n-1)*4+1],data[(n-1)*4+2],data[(n-1)*4+3].
				// var pos =[( i-1 )*200]+( j-1 )]*4;*/
					//console.log('this.data.data[(y * 4 * this.data.width) + (x * 4)', this.data.data[(y * 4 * this.data.width) + (x * 4)]);
					let red = this.data.data[(y * 4 * this.data.width) +  (x * 4)];
					let green = this.data.data[(y * 4 * this.data.width) +  (x * 4 + 1)];
					let blue = this.data.data[(y * 4 * this.data.width) +  (x * 4 + 2)];
					let brightness = this.calculateRelativeBrightness(red, green, blue);
					row.push(brightness);
			}
			this.mappedImageArray.push(row);
		}
		console.log(this.mappedImageArray)
	}
	calculateRelativeBrightness(red, green, blue) {
		return Math.sqrt(
			(red * red) * 0.299 +
			(green * green) * 0.587 +
			(blue * blue) * 0.114
		) / 100
	}
}

window.onload = () => {
	new App();
};