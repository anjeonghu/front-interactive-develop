
class App {
	constructor() {
		this.canvas = document.createElement('canvas');
		document.body.appendChild(this.canvas);
		this.ctx = this.canvas.getContext('2d');
		this.size = 0;
		this.number = 0;
		this.scale = 10;
		this.hue = 0;
		this.x = 300;
		this.y = 300;

		window.addEventListener('resize', this.resize.bind(this), false);
		this.resize();
		this.animate();
	}
	resize() {
		this.stageWidth = window.innerWidth;
		this.stageHeight = window.innerHeight;

		this.canvas.width = this.stageWidth;
		this.canvas.height = this.stageHeight;

	}

	draw () {
		// angle 값에 따라 다양한 모양으로 그려진다.
		this.angle = this.number * 500;
		this.radius = this.scale * Math.sqrt(this.number);

		// 원의 좌표 구하기
		// this.radius * Math.cos(this.angle)
		this.x = this.radius * Math.cos(this.angle) + this.canvas.width/2;
		this.y = this.radius * Math.sin(this.angle) + this.canvas.height/2;


		// hue rotation 기법으로 색상에 변화를 준다.
		this.ctx.fillStyle = `hsl(${this.hue}, 100%, 50%)`;
		this.ctx.strokeStyle = 'blue';
		this.ctx.beginPath();
		this.ctx.arc(this.x, this.y, 20, 0, Math.PI * 2);
		this.ctx.closePath();
		this.ctx.fill();
		this.ctx.stroke();
	}

	animate () {
		requestAnimationFrame(this.animate.bind(this));
		// this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
		if (this.number > 300) return;
		this.draw();
		this.hue++;
		this.number++;

	}
}

window.onload = () => {
	new App();
};