export class Bubbles {
	constructor (canvas, player) {
		this.canvas = canvas;
		this.player = player;
		this.x = Math.random() * canvas.width;
		this.y = canvas.height + 100;
		this.radius = 50;
		this.speed = Math.random() * 5 + 1;
		this.distance = 0;
		this.counted = false;
		this.sound = Math.random() <= 0.5 ? 'sound1': 'sound2';
	}
	update () {
		this.y -= this.speed;
		const dx = Math.abs(this.player.x - this.x);
		const dy = Math.abs(this.player.y - this.y);
		this.distance = Math.sqrt(dx**2 + dy**2);
	}
	draw (ctx) {
		ctx.fillStyle = 'rgba(255,255,255,0.5)';
		ctx.beginPath();
		ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2);
		ctx.closePath();
		ctx.fill();
		ctx.lineWidth = 1;
		ctx.strokeStyle = 'white';
		ctx.stroke();
	}
}