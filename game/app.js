import {Player} from "./Player.js";
import {Bubbles} from "./Bubbles.js";

class App {
	constructor() {
		this.canvas = document.createElement('canvas');
		document.body.appendChild(this.canvas);
		this.ctx = this.canvas.getContext('2d');
		this.ctx.font = '50px Georgia';
		this.canvas.width = 800;
		this.canvas.height = 500;
		this.score = 0;
		this.gameFrame = 0;
		this.player = new Player();
		this.bubblesArray = [];
		this.bubblePop1 = document.createElement('audio');
		this.bubblePop2 = document.createElement('audio');

		this.bubblePop1.src = './sounds/bubbles-single1.wav';
		this.bubblePop2.src = './sounds/Plop.ogg';
		this.bgImage = new Image();
		this.bgImage.src ='./images/see.jpg';

		this.bgImage.addEventListener('load', () => {
			this.ctx.drawImage(this.bgImage, 0, 0, this.canvas.width, this.canvas.height);
		});
		this.mouse = {
			x: this.canvas.width /2,
			y: this.canvas.height/2,
			click: false
		};

		this.canvas.addEventListener('mousedown', this.mouseDown.bind(this))
		this.canvas.addEventListener('mouseup', this.mouseUp.bind(this))

		this.resize();
		this.animate();
	}
	resize() {
	}
	mouseDown (e) {
		this.mouse.click = true;
		this.mouse.x = e.offsetX;
		this.mouse.y = e.offsetY;
	}
	mouseUp (e) {
		this.mouse.click = false;
	}
	handleBubbles () {
		if ((this.gameFrame % 50) === 0) {
			this.bubblesArray.push(new Bubbles(this.canvas, this.player))
		}
		for (let i = 0; i < this.bubblesArray.length; i++) {
			this.bubblesArray[i].update();
			this.bubblesArray[i].draw(this.ctx);
		}
		for (let i = 0; i < this.bubblesArray.length; i++) {
			if (this.bubblesArray[i].y < 0 - this.bubblesArray[i].radius * 2) {
				this.bubblesArray.splice(i, 1);
			}

			// collision
			if (this.bubblesArray[i].distance < this.bubblesArray[i].radius + this.player.radius) {
				if(!this.bubblesArray[i].counted) {
					if (this.bubblesArray[i].sound === 'sound1') {
						this.bubblePop1.play().then(r => {});
					} else {
						this.bubblePop2.play().then(r => {});
					}
					this.score++;
					this.bubblesArray[i].counted = true;
					this.bubblesArray.splice(i, 1);
				}
			}
		}
	}
	animate () {
		requestAnimationFrame(this.animate.bind(this));
		this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
		this.ctx.drawImage(this.bgImage, 0, 0, this.canvas.width, this.canvas.height);
		this.handleBubbles();
		this.player.update(this.mouse);
		this.player.draw(this.ctx, this.mouse);
		this.ctx.fillStyle = 'white';
		this.ctx.fillText('score ' + this.score, 10, 50);
		this.gameFrame++;
	}
}

window.onload = () => {
	new App();
};