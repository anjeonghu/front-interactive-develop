export class Player {
	constructor () {
		this.x = 0;
		this.y = 0;
		this.radius = 128;
		this.angle = 0;
		this.framX = 0;
		this.framY = 0;
		this.frame = 0;
		this.spriteWidth = 400;
		this.spriteHeight = 327;
		this.playerLeft = new Image();
		this.playerRight = new Image();
		this.playerLeft.src = './images/fish_swim_left.png';
		this.playerRight.src = './images/fish_swim_right.png';
	}
	update (mouse) {
		const dx = this.x - mouse.x;
		const dy = this.y - mouse.y;
		// this.angle = Math.atan2(dy, dx);
		this.angle = Math.tan(dy /dx);

		if (mouse.x !== this.x) {
			this.x -= dx / 30;
		}
		if (mouse.y !== this.y) {
			this.y -= dy / 30;
		}
	}
	draw (ctx, mouse) {

		if(mouse.click) {
			ctx.lineWidth = 0.2;
			ctx.beginPath();
			ctx.moveTo(this.x, this.y);
			ctx.lineTo(mouse.x, mouse.y);
			ctx.stroke();
		}
		ctx.fillStyle = `red`;
		ctx.strokeStyle = 'blue';
		ctx.beginPath();
		ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();

		ctx.save();
		ctx.translate(this.x, this.y);
		ctx.rotate(this.angle);
		let degree = this.angle * 180 / Math.PI
		console.log(degree);

		if (this.x >= mouse.x) {
			// 왼쪽 으로
			ctx.drawImage(this.playerLeft, 0, 0, 256, 256, -128, -128, 256, 256);

		} else {
			// 오른쪽으로
			// ctx.rotate(Math.PI - this.angle);

			ctx.drawImage(this.playerRight, 0, 0, 256, 256, -128, -128, 256, 256);
		}

		ctx.restore();
	}
}