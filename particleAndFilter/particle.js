export class Particle {
	constructor(x, y, size, color, weight) {
		this.x = x;
		this.y = y;
		this.size = size;
		this.color = color;
		this.weight = weight;
		this.maxSize = 130;
	}

	draw (ctx) {
		ctx.beginPath();
		ctx.arc(this.x, this.y, this.size, 0, Math.PI * 2, false)
		ctx.fillStyle = this.color;
		ctx.closePath();
		ctx.fill();
	}

	update (ctx, canvas) {
		// this.size -= 0.05;
		// if (this.size < 0) {
		// 	this.x = (Math.random() * 20) - 10;
		// 	this.y = (Math.random() * 20) - 10;
		// 	this.size = (Math.random() * 10) + 2;
		// 	this.weight = (Math.random() * 2) - 0.5;
		// }
		// this.y += this.weight;
		// this.weight += 0.2;
		//
		// if (this.y > canvas.height - this.size) {
		// 	this.weight *= -1;
		// }
		if (this.size < this.maxSize) {
			this.size += 2;
			this.draw(ctx);
		}
	}
}