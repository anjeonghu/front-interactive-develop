import {Particle} from "./particle.js";

class App {
	constructor() {
		this.canvas = document.createElement('canvas');
		this.ctx = this.canvas.getContext('2d');
		this.hue = 1;
		this.numberOfParticles = 50;
		document.body.appendChild(this.canvas);


		window.addEventListener('resize', this.resize.bind(this), false);
		this.resize();
		this.animate();
	}
	resize() {
		this.stageWidth = window.innerWidth;
		this.stageHeight = window.innerHeight;

		console.log(this.stageWidth / 20 )
		this.numberOfParticles = Math.floor(this.stageWidth / 15);

		this.canvas.width = this.stageWidth;
		this.canvas.height = this.stageHeight;

		this.init();
	}

	init () {
		this.particleArray = [];
		for (let i = 0; i < this.numberOfParticles; i++) {
			let x = Math.random() * this.canvas.width;
			let y = Math.random() * this.canvas.height;
			let size = (Math.random() * 35) + 15;
			// let color = `hsl(${this.hue}, 100%, 50%)`;
			let color = `pink`;
			let weight = (Math.random() * 10) + 2;
			this.particleArray.push(new Particle(x, y, size, color, weight));
			this.hue++;
		}
	}

	animate () {
		requestAnimationFrame(this.animate.bind(this));
		//this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
		for (let i = 0; i < this.particleArray.length; i++) {
			this.particleArray[i].update(this.ctx, this.canvas);
		}
	}
}

window.onload = () => {
	new App();
};