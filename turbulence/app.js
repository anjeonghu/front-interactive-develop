const ele = document.querySelector('.turbulence');
const turbulence = document.querySelector('feTurbulence');
let verticalFrequency = 0.00001;
turbulence.setAttribute('baseFrequency', verticalFrequency + '0.00001');
const steps = 30;
const interval = 10;

for (let i = 0; i < steps; i++) {
	setTimeout(function () {
		verticalFrequency += 0.002;
		turbulence.setAttribute('baseFrequency', verticalFrequency +  '0.00001')
	}, i * interval)

}