

const APPLE_EYE_ELEMENT_LEFT = document.querySelector('.apple_of_eye.left');
const APPLE_EYE_ELEMENT_RIGHT = document.querySelector('.apple_of_eye.right');
const box_1 = APPLE_EYE_ELEMENT_LEFT.getBoundingClientRect();
const box_2 = APPLE_EYE_ELEMENT_RIGHT.getBoundingClientRect();
const l_cx = box_1.x + (box_1.width / 2);
const l_cy = box_1.y + (box_1.height / 2);
const r_cx = box_1.x + (box_1.width / 2);
const r_cy = box_1.y + (box_1.height / 2);


window.addEventListener("mousemove", (e) => {
	const x = e.clientX,
		y = e.clientY;


	const radian_l = Math.atan2(l_cy - y, l_cx - x) ;
	const radian_r = Math.atan2(y - l_cy, x - l_cx) ;
	const degree_l = radians_to_degrees(radian_l);
	const degree_r = radians_to_degrees(radian_r);
	APPLE_EYE_ELEMENT_LEFT.style.transform = `rotateZ(${degree_l}deg)`;
	APPLE_EYE_ELEMENT_RIGHT.style.transform = `rotateZ(${degree_r}deg)`;
});


function radians_to_degrees(radians)
{
	var pi = Math.PI;
	return radians * (180/pi);
}