export class Particle {
	constructor(x, y, color, canvas, png) {
		this.x = x + canvas.width/2 - png.width * 2;
		this.y = y + canvas.height/2 - png.height * 2;
		this.size = 2;
		this.color = color;
		this.baseX = this.x;
		this.baseY = this.y;
		this.density = (Math.random() * 10) + 2;
	}

	draw (ctx) {
		ctx.beginPath();
		ctx.arc(this.x, this.y, this.size, 0, Math.PI * 2, false)
		ctx.fillStyle = this.color;
		ctx.fill();
	}

	update (mouse, canvas, ctx) {
		// collision detection
		let dx = mouse.x - this.x;
		let dy = mouse.y - this.y;
		let distance = Math.sqrt(dx*dx + dy*dy);
		let forceDirectionX = dx / distance; // Math.cos()
		let forceDirectionY = dy / distance; // Math.sin()

		// max distance, past that the force will be 0
		const maxDistance = 100;

		// force - distance 가 100에 가까울수록 force 는 0에 가까워진다.
		let force = (maxDistance - distance) / maxDistance;
		if (force < 0 ) force = 0;

		let directionX = (forceDirectionX * force * this.density * 0.6);
		let directionY = (forceDirectionY * force * this.density * 0.6);

		if (distance < mouse.radius + this.size) {
			this.x -= directionX;
			this.y -= directionY;
		} else {
			if (this.x !== this.baseX) {
				let dx = this.x - this.baseX;
				this.x -= dx/20;
			}
			if (this.y !== this.baseY) {
				let dy = this.y - this.baseY;
				this.y -= dy/20;
			}
		}
		this.draw(ctx);
	}
}