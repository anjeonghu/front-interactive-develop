import {Particle} from './particle.js'
import ImageBase64 from './const.js'

class App {
	constructor() {
		this.canvas = document.createElement('canvas');
		document.body.appendChild(this.canvas);
		this.ctx = this.canvas.getContext('2d');

		this.pixelRatio = window.devicePixelRatio > 1 ? 2 : 1;
		this.particles = [];
		this.isDown = false;

		this.mouse = {
			x: null,
			y: null,
			radius: 100
		};
		this.png = new Image();
		this.png.src = ImageBase64;
		this.png.onload = () => {
			this.resize();
			this.ctx.drawImage(this.png, 0, 0);
			this.drawImage();
			this.init();
			this.animate();
		};

		window.addEventListener('resize', this.resize.bind(this), false);

		window.addEventListener('pointerdown', this.onDown.bind(this), false);
		window.addEventListener('pointermove', this.onMove.bind(this), false);
		window.addEventListener('pointerup', this.onUp.bind(this), false);
	}
	init () {
		for (let y = 0, y2 = this.data.height; y < y2; y++) {
			for (let x = 0, x2 = this.data.width; x < x2; x++) {
				//*
				// The first pixel information is RGBA(data[0],data[1],data[2],data[3]).
				// The second pixel information is RGBA(data[4],data[5],data[6],data[7]).
				// The N-th pixel information is RGBA(data[(n-1)*4],data[(n-1)*4+1],data[(n-1)*4+2],data[(n-1)*4+3].
				// In addition, since the pixel area is an area, it is wide and high. The above formula is suitable for locating a pixel in a single row. So when calculating the pixels, we should take into account the location in the whole image area:
				// The above figure is an example. The width and height of the image are 200, if each pixel is a row and a column. The image has 200 rows and 200 columns. So the initial position information of the pixel in column j of row i is obtained as follows:
				// var pos =[( i-1 )*200]+( j-1 )]*4;*/
				if (this.data.data[(y * 4 * this.data.width) + (x * 4)] + 3 > 128) {
					console.log('this.data.data[(y * 4 * this.data.width) + (x * 4)', this.data.data[(y * 4 * this.data.width) + (x * 4)]);
					let positionX = x;
					let positionY = y;
					let color = `rgba(${this.data.data[(y * 4 * this.data.width) +  (x * 4)]}, ${this.data.data[(y * 4 * this.data.width) +  (x * 4)] + 1}, ${this.data.data[(y * 4 * this.data.width) +  (x * 4)] + 2}, ${this.data.data[(y * 4 * this.data.width) +  (x * 4)] + 3})`;
					this.particles.push(new Particle(positionX * 4, positionY * 4, color, this.canvas, this.png))
				}
			}
		}
	}
	resize () {
		this.stageWidth = document.body.clientWidth;
		this.stageHeight = document.body.clientHeight;

		this.canvas.width = this.stageWidth;
		this.canvas.height = this.stageHeight;
	}
	drawImage () {
		let imageWidth = this.png.width;
		let imageHeight = this.png.height;
		this.data = this.ctx.getImageData(0, 0, imageWidth, imageHeight)
		this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
	}
	animate () {
		requestAnimationFrame(this.animate.bind(this));
		this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
		this.ctx.fillStyle = 'rgba(0, 0, 0, .05)';
		this.ctx.fillRect(0, 0, innerWidth, innerHeight);

		for (let i = 0; i < this.particles.length; i ++) {
			this.particles[i].update(this.mouse, this.canvas, this.ctx);
		}
	}
	onDown (e) {
		this.isDown = true;
		this.mouse.x = e.clientX;
		this.mouse.y = e.clientY;
	}
	onMove (e) {
		if (this.isDown) {
			this.mouse.x = e.clientX;
			this.mouse.y = e.clientY;
		}
	}
	onUp (e) {
		this.isDown = false;
		//
		this.mouse.x = -5000;
		this.mouse.y = -5000;
	}
}

window.onload = () => {
	new App();
};