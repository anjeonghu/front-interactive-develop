
class App {
	constructor() {
		this.canvas = document.createElement('canvas');
		document.body.appendChild(this.canvas);
		this.ctx = this.canvas.getContext('2d');

		this.pixelRatio = window.devicePixelRatio > 1 ? 2 : 1;

		this.x = 0;
		this.y = 0;
		this.moveX = -5000;
		this.moveY = -5000;
		this.speed = 0.125;
		this.isDown = false;

		window.addEventListener('resize', this.resize.bind(this), false);
		this.resize();

		window.addEventListener('pointerdown', this.onDown.bind(this), false);
		window.addEventListener('pointermove', this.onMove.bind(this), false);
		window.addEventListener('pointerup', this.onUp.bind(this), false);

		requestAnimationFrame(this.animate.bind(this));
	}
	resize () {
		this.stageWidth = document.body.clientWidth;
		this.stageHeight = document.body.clientHeight;

		this.canvas.width = this.stageWidth * this.pixelRatio;
		this.canvas.height = this.stageHeight * this.pixelRatio;

		this.ctx.scale(this.pixelRatio, this.pixelRatio);

	}
	animate() {
		requestAnimationFrame(this.animate.bind(this));
		if(!this.isDown) return;
		let dx, dy;
		if (!this.x|| !this.y) {
			this.x = this.moveX;
			this.y = this.moveY;
		} else {
			// 두점 사이의 거리를 구하고 8/1 속도로 이동한다.
			dx = (this.moveX - this.x) * this.speed;
			dy = (this.moveY - this.y) * this.speed;
			if (Math.abs(dx) + Math.abs(dy) < 0.1) {
				this.x = this.moveX;
				this.y = this.moveY;
			} else {
				this.x += dx;
				this.y += dy;
			}
		}
		this.ctx.clearRect(0, 0, this.stageWidth, this.stageHeight);
		this.ctx.beginPath();
		this.ctx.strokeStyle = '#ffffff'
		this.ctx.arc(this.x, this.y, 50, 0, 2 * Math.PI);
		this.ctx.stroke();
	}
	onDown (e) {
		this.isDown = true;
		this.moveX = e.clientX;
		this.moveY = e.clientY;
	}
	onMove (e) {
		if (this.isDown) {
			this.moveX = e.clientX;
			this.moveY = e.clientY;
		}
	}
	onUp (e) {
		this.isDown = false;

		this.moveX = -5000;
		this.moveY = -5000;
	}
}

window.onload = () => {
	new App();
};