import {Particle} from './particle.js'

class App {
	constructor() {
		this.canvas = document.createElement('canvas');
		document.body.appendChild(this.canvas);
		this.ctx = this.canvas.getContext('2d');

		this.pixelRatio = window.devicePixelRatio > 1 ? 2 : 1;

		this.isDown = false;

		window.addEventListener('resize', this.resize.bind(this), false);
		this.resize();
		this.mouse = {
			x: null,
			y: null,
			radius: (this.canvas.height / 80) * (this.canvas.width / 80)
		}
		this.particles = [];
		let numberOfParticles = (this.canvas.height * this.canvas.width) / 9000;

		for (let i = 0; i < numberOfParticles * 2; i++) {
			let size = (Math.random() * 5) + 1;
			let x = (Math.random() * ((this.canvas.width - size * 2) - (size * 2)) + size * 2);
			let y= (Math.random() * ((this.canvas.width - size * 2) - (size * 2)) + size * 2);

			console.log(x, y)
			let directionX = (Math.random() * 5) - 2.5;
			let directionY = (Math.random() * 5) - 2.5;
			let color = '#8c5523';
			let particle = new Particle(x, y, directionX, directionY, size, color);

			this.particles.push(particle);
		}
		window.addEventListener('pointerdown', this.onDown.bind(this), false);
		window.addEventListener('pointermove', this.onMove.bind(this), false);
		window.addEventListener('pointerup', this.onUp.bind(this), false);

		this.animate();
	}
	resize () {
		this.stageWidth = document.body.clientWidth;
		this.stageHeight = document.body.clientHeight;

		this.canvas.width = this.stageWidth;
		this.canvas.height = this.stageHeight;
	}
	animate () {
		requestAnimationFrame(this.animate.bind(this));
		this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

		for (let i = 0; i < this.particles.length; i ++) {
			this.particles[i].update(this.mouse, this.canvas, this.ctx);
		}
		this.connect();
	}
	connect () {
		for (let i = 0; i < this.particles.length - 1; i ++) {
			for (let j = i; j < this.particles.length; j++) {
				let distance = ((this.particles[i].x - this.particles[j].x) * (this.particles[i].x - this.particles[j].x))
				+ ((this.particles[i].y - this.particles[j].y) * (this.particles[i].y - this.particles[j].y))

				if (distance < (this.canvas.width / 7) * (this.canvas.height / 7)) {
					let opacity = 1 - (distance / 20000);
					this.ctx.beginPath();
					this.ctx.strokeStyle = `rgba(140, 85, 31, ${opacity})`;
					this.ctx.lineWidth = 1;
					this.ctx.moveTo(this.particles[i].x, this.particles[i].y);
					this.ctx.lineTo(this.particles[j].x, this.particles[j].y);
					this.ctx.stroke();
				}
			}
		}
	}
	onDown (e) {
		this.isDown = true;
		this.mouse.x = e.clientX;
		this.mouse.y = e.clientY;
	}
	onMove (e) {
		if (this.isDown) {
			this.mouse.x = e.clientX;
			this.mouse.y = e.clientY;
		}
	}
	onUp (e) {
		this.isDown = false;
		//
		this.mouse.x = -5000;
		this.mouse.y = -5000;
	}
}

window.onload = () => {
	new App();
};